package lolapi

import (
	"fmt"
	"strings"
)

// League information for a particular summoner
// or team.
type LeagueDto struct {
	// The requested league entries.
	Entries []LeagueEntryDto `json:"entries"`
	// This name is an internal place-holder name
	// only. Display and localization of names in
	// the game client are handled client-side.
	Name string `json:"name"`
	// Specifies the relevant participant that is
	// a member of this league (i.e., a requested
	// summoner ID, a requested team ID, or the ID
	// of a team to which one of the requested
	// summoners belongs). Only present when full
	// league is requested so that participant's
	// entry can be identified. Not present when
	// individual entry is requested.
	ParticipantID string `json:"participantId"`
	// The league's queue type. (Legal values:
	// RANKED_SOLO_5x5, RANKED_TEAM_3x3,
	// RANKED_TEAM_5x5)
	Queue string `json:"queue"`
	// The league's tier. (Legal values:
	// CHALLENGER, MASTER, DIAMOND, PLATINUM,
	// GOLD, SILVER, BRONZE)
	Tier string `json:"tier"`
}

// This object contains league participant information
// representing a summoner or team.
type LeagueEntryDto struct {
	// The league division of the participant.
	Division string `json:"division"`
	// Specifies if the participant is fresh blood.
	// New to the league.
	IsNew bool `json:"isFreshBlood"`
	// Specifies if the participant is on a hot streak.
	OnHotStreak bool `json:"isHotStreak"`
	// Specifies if the participant is inactive.
	Inactive bool `json:"isInactive"`
	// Specifies if the participant is a veteran.
	IsVeteran bool `json:"isVeteran"`
	// The league points of the participant.
	LeaguePoints int `json:"leaguePoints"`
	// The number of losses for the participant.
	Losses int `json:"losses"`
	// Mini series data for the participant. Only
	// present if the participant is currently in a
	// mini series.
	MiniSeries MiniSeriesDto `json:"miniSeries"`
	// The ID of the participant (i.e., summoner or
	// team) represented by this entry.
	ID string `json:"playerOrTeamId"`
	// The name of the the participant (i.e., summoner
	// or team) represented by this entry.
	Name string `json:"playerOrTeamName"`
	// The number of wins for the participant.
	Wins int `json:"wins"`
}

// This object contains mini series information.
type MiniSeriesDto struct {
	// Number of current losses in the mini series.
	Losses int `json:"losses"`
	// String showing the current, sequential mini series
	// progress where 'W' represents a win, 'L' represents
	// a loss, and 'N' represents a game that hasn't been
	// played yet.
	Progress string `json:"progress"`
	// Number of wins required for promotion.
	Target int `json:"target"`
	// Number of current wins in the mini series.
	Wins int `json:"wins"`
}

// Fetches league data for Challenger tier solo queue.
func (api *LolApi) ChallengerSolo() (*LeagueDto, *RestError) {
	res := new(LeagueDto)
	url := fmt.Sprintf(CHALLENGER_LEAGUE_URL, api.region.url, api.region.code)
	options := make(map[string]string)
	options["type"] = "RANKED_SOLO_5x5"

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

// Fetches league data for Challenger tier ranked 5v5 queue.
func (api *LolApi) ChallengerTeam5() (*LeagueDto, *RestError) {
	res := new(LeagueDto)
	url := fmt.Sprintf(CHALLENGER_LEAGUE_URL, api.region.url, api.region.code)
	options := make(map[string]string)
	options["type"] = "RANKED_TEAM_5x5"

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

// Fetches league data for Challenger tier ranked 3v3 queue.
func (api *LolApi) ChallengerTeam3() (*LeagueDto, *RestError) {
	res := new(LeagueDto)
	url := fmt.Sprintf(CHALLENGER_LEAGUE_URL, api.region.url, api.region.code)
	options := make(map[string]string)
	options["type"] = "RANKED_TEAM_3x3"

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

// Fetches league data for Master tier solo queue.
func (api *LolApi) MasterSolo() (*LeagueDto, *RestError) {
	res := new(LeagueDto)
	url := fmt.Sprintf(MASTER_LEAGUE_URL, api.region.url, api.region.code)
	options := make(map[string]string)
	options["type"] = "RANKED_SOLO_5x5"

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

// Fetches league data for Master tier ranked 5v5 queue.
func (api *LolApi) MasterTeam5() (*LeagueDto, *RestError) {
	res := new(LeagueDto)
	url := fmt.Sprintf(MASTER_LEAGUE_URL, api.region.url, api.region.code)
	options := make(map[string]string)
	options["type"] = "RANKED_TEAM_5x5"

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

// Fetches league data for Master tier ranked 3v3 queue.
func (api *LolApi) MasterTeam3() (*LeagueDto, *RestError) {
	res := new(LeagueDto)
	url := fmt.Sprintf(MASTER_LEAGUE_URL, api.region.url, api.region.code)
	options := make(map[string]string)
	options["type"] = "RANKED_TEAM_3x3"

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

// Fetches league data for given summoner ids.
func (api *LolApi) SummonersLeagues(ids []string) ([]*LeagueDto, *RestError) {
	res := make(map[string][]*LeagueDto, len(ids))
	url := fmt.Sprintf(SUMMONER_LEAGUE_URL, api.region.url,
		api.region.code, strings.Join(ids, ","))

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	actualRes := make([]*LeagueDto, 0, len(res))
	for _, v := range res {
		for _, l := range v {
			actualRes = append(actualRes, l)
		}
	}

	return actualRes, err
}

// Get league information for a given summoner id.
func (api *LolApi) SummonerLeague(id string) (*LeagueDto, *RestError) {
	res, err := api.SummonersLeagues([]string{id})
	if err != nil {
		return nil, err
	}

	return res[0], nil
}

// Fetches league data for given team ids.
func (api *LolApi) TeamsLeagues(ids []string) ([]*LeagueDto, *RestError) {
	res := make(map[string][]*LeagueDto, len(ids))
	url := fmt.Sprintf(TEAM_LEAGUE_URL, api.region.url,
		api.region.code, strings.Join(ids, ","))

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	actualRes := make([]*LeagueDto, 0, len(res))
	for _, v := range res {
		for _, l := range v {
			actualRes = append(actualRes, l)
		}
	}

	return actualRes, err
}

// Fetches league data for given team id.
func (api *LolApi) TeamLeague(id string) (*LeagueDto, *RestError) {
	res, err := api.TeamsLeagues([]string{id})
	if err != nil {
		return nil, err
	}

	return res[0], nil
}
