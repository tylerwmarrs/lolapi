package lolapi

import (
	"fmt"
)

// This object contains single Champion Mastery information for
// player and champion combination.
type ChampionMasteryDto struct {
	// Champion ID for this entry.
	ChampionID int `json:"championId"`
	// Champion level for specified player and champion combination.
	ChampionLevel int `json:"championLevel"`
	// Total number of champion points for this player and champion
	// combination - they are used to determine championLevel.
	ChampionPoints int64 `json:"championPoints"`
	// Number of points earned since current level has been achieved.
	// Zero if player reached maximum champion level for this champion.
	PointsSinceLastLevel int64 `json:"championPointsSinceLastLevel"`
	// Number of points needed to achieve next level. Zero if player
	// reached maximum champion level for this champion.
	PointsUntilNextLevel int64 `json:"championPointsUntilNextLevel"`
	// Is chest granted for this champion or not in current season.
	ChestGranted bool `json:"chestGranted"`
	// The highest grade of this champion of current season.
	HighestGrade string `json:"highestGrade"`
	// Last time this champion was played by this player - in Unix
	// milliseconds time format.
	LastPlayed EpochMillisecond `json:"lastPlayTime"`
	// Player ID for this entry.
	PlayerID int64 `json:"playerId"`
}

// Fetches champion mastery for a given player's id.
// Results are a map of int (champion id) -> ChampionMasteryDto.
func (api *LolApi) ChampionMastery(id string) (map[int]*ChampionMasteryDto, *RestError) {
	res := make([]*ChampionMasteryDto, 0)
	url := fmt.Sprintf(CHAMPION_MASTERY_URL, api.region.url,
		api.region.platformID, id)

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	actualRes := make(map[int]*ChampionMasteryDto, len(res))
	for _, v := range res {
		actualRes[v.ChampionID] = v
	}

	return actualRes, err
}
