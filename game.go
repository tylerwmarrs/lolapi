package lolapi

import "fmt"

// Riot games RecentGamesDto for the LoL API.
type RecentGamesDto struct {
	Games      []GameDto `json:"games"`
	SummonerID int       `json:"summonerId"`
}

// Riot games GameDTO for the LoL API.
type GameDto struct {
	// Champion ID associated with game.
	ChampionID int `json:"championId"`
	// Date that end game data was recorded, specified
	// as epoch milliseconds.
	CreateDate EpochMillisecond `json:"createDate"`
	// Other players associated with the game.
	Players []PlayerDto `json:"fellowPlayers"`
	// Game ID
	GameID int `json:"gameId"`
	// Game mode. (Legal values: CLASSIC, ODIN, ARAM,
	// TUTORIAL, ONEFORALL, ASCENSION, FIRSTBLOOD, KINGPORO)
	GameMode string `json:"gameMode"`
	// Game type. (Legal values: CUSTOM_GAME, MATCHED_GAME,
	// TUTORIAL_GAME)
	GameType string `json:"gameType"`
	Invalid  bool   `json:"invalid"`
	IpEarned int    `json:"ipEarned"`
	Level    int    `json:"level"`
	// Static ID of map. See static API.
	MapId int `json:"mapId"`
	// Summoner spell 1. See static API.
	Spell1 int `json:"spell1"`
	// Summoner spell 2. See static API.
	Spell2 int `json:"spell2"`
	// Statistics associated with the game for this summoner.
	Stats RawStatsDto `json:"stats"`
	// Game sub-type. (Legal values: NONE, NORMAL, BOT,
	// RANKED_SOLO_5x5, RANKED_PREMADE_3x3, RANKED_PREMADE_5x5,
	// ODIN_UNRANKED, RANKED_TEAM_3x3, RANKED_TEAM_5x5,
	// NORMAL_3x3, BOT_3x3, CAP_5x5, ARAM_UNRANKED_5x5,
	// ONEFORALL_5x5, FIRSTBLOOD_1x1, FIRSTBLOOD_2x2, SR_6x6,
	// URF, URF_BOT, NIGHTMARE_BOT, ASCENSION, HEXAKILL,
	// KING_PORO, COUNTER_PICK, BILGEWATER)
	SubType string `json:"subType"`
	// Team ID associated with game.
	// Team ID 100 is blue team. Team ID 200 is purple team.
	TeamID TeamID `json:"teamId"`
}

// Riot game's RawStatsDto for LoL API.
type RawStatsDto struct {
	Assists int `json:"assists"`
	// Number of enemy inhibitors killed.
	InhibsKilled                    int `json:"barracksKilled"`
	BountyLevel                     int `json:"bountyLevel"`
	ChampionsKilled                 int `json:"championsKilled"`
	CombatPlayerScore               int `json:"combatPlayerScore"`
	ConsumablesPurchased            int `json:"consumablesPurchased"`
	DamageDealtPlayer               int `json:"damageDealtPlayer"`
	DoubleKills                     int `json:"doubleKills"`
	FirstBlood                      int `json:"firstBlood"`
	Gold                            int `json:"gold"`
	GoldEarned                      int `json:"goldEarned"`
	GoldSpent                       int `json:"goldSpent"`
	Item0                           int `json:"item0"`
	Item1                           int `json:"item1"`
	Item2                           int `json:"item2"`
	Item3                           int `json:"item3"`
	Item4                           int `json:"item4"`
	Item5                           int `json:"item5"`
	Item6                           int `json:"item6"`
	ItemsPurchased                  int `json:"itemsPurchased"`
	KillingSprees                   int `json:"killingSprees"`
	LargestCrit                     int `json:"largestCriticalStrike"`
	LargestKillingSpree             int `json:"largestKillingSpree"`
	LargestMultiKill                int `json:"largestMultiKill"`
	LegendaryItemsCreated           int `json:"legendaryItemsCreated"`
	Level                           int `json:"level"`
	MagicDamageDealtPlayer          int `json:"magicDamageDealtPlayer"`
	MagicDamageChamps               int `json:"magicDamageDealtToChampions"`
	MagicDamageTaken                int `json:"magicDamageTaken"`
	MinionsDenied                   int `json:"minionsDenied"`
	MinionsKilled                   int `json:"minionsKilled"`
	NeutralMinionsKilled            int `json:"neutralMinionsKilled"`
	NeutralMinionsKilledEnemyJungle int `json:"neutralMinionsKilledEnemyJungle"`
	NeutralMinionsKilledYourJungle  int `json:"neutralMinionsKilledYourJungle"`
	// Flag specifying if the summoner got the killing blow on the nexus.
	NexusKilled                    bool `json:"nexusKilled"`
	NodeCapture                    int  `json:"nodeCapture"`
	NodeCaptureAssist              int  `json:"nodeCaptureAssist"`
	NodeNeutralize                 int  `json:"nodeNeutralize"`
	NodeNeutralizeAssist           int  `json:"nodeNeutralizeAssist"`
	Deaths                         int  `json:"numDeaths"`
	NumItemsBought                 int  `json:"numItemsBought"`
	ObjectivePlayerScore           int  `json:"objectivePlayerScore"`
	PentaKills                     int  `json:"pentaKills"`
	PhysicalDamageDealtPlayer      int  `json:"physicalDamageDealtPlayer"`
	PhysicalDamageDealtToChampions int  `json:"physicalDamageDealtToChampions"`
	PhysicalDamageTaken            int  `json:"physicalDamageTaken"`
	// Player position (Legal values: TOP(1), MIDDLE(2), JUNGLE(3), BOT(4))
	PlayerPosition int `json:"playerPosition"`
	// Player role (Legal values: DUO(1), SUPPORT(2), CARRY(3), SOLO(4))
	PlayerRole       int `json:"playerRole"`
	QuadraKills      int `json:"quadraKills"`
	SightWardsBought int `json:"sightWardsBought"`
	// Number of times first champion spell was cast.
	Spell1Cast int `json:"spell1Cast"`
	// Number of times second champion spell was cast.
	Spell2Cast int `json:"spell2Cast"`
	// Number of times third champion spell was cast.
	Spell3Cast int `json:"spell3Cast"`
	// Number of times fifth champion spell was cast.
	Spell4Cast int `json:"spell4Cast"`
	// Number of times sumoner spell 1 was cast.
	SummonerSpell1Cast int `json:"summonSpell1Cast"`
	// Number of times sumoner spell 2 was cast.
	SummonerSpell2Cast          int `json:"summonSpell2Cast"`
	SuperMonsterKilled          int `json:"superMonsterKilled"`
	Team                        int `json:"team"`
	TeamObjective               int `json:"teamObjective"`
	TimePlayed                  int `json:"timePlayed"`
	TotalDamageDealt            int `json:"totalDamageDealt"`
	TotalDamageDealtToChampions int `json:"totalDamageDealtToChampions"`
	TotalDamageTaken            int `json:"totalDamageTaken"`
	TotalHeal                   int `json:"totalHeal"`
	TotalPlayerScore            int `json:"totalPlayerScore"`
	TotalScoreRank              int `json:"totalScoreRank"`
	TotalTimeCrowdControlDealt  int `json:"totalTimeCrowdControlDealt"`
	TotalUnitsHealed            int `json:"totalUnitsHealed"`
	TripleKills                 int `json:"tripleKills"`
	TrueDamageDealtPlayer       int `json:"trueDamageDealtPlayer"`
	TrueDamageDealtToChampions  int `json:"trueDamageDealtToChampions"`
	TrueDamageTaken             int `json:"trueDamageTaken"`
	TurretsKilled               int `json:"turretsKilled"`
	UnrealKills                 int `json:"unrealKills"`
	VictoryPointTotal           int `json:"victoryPointTotal"`
	VisionWardsBought           int `json:"visionWardsBought"`
	WardsKilled                 int `json:"wardKilled"`
	WardsPlaced                 int `json:"wardPlaced"`
	// Whether or not the summoner won this game.
	Win bool `json:"win"`
}

// Riot game's PlayerDto for LoL API.
// It contains fellow team mates information.
type PlayerDto struct {
	// Champion id associated with player.
	ChampionID int `json:"championId"`
	// Summoner id associated with player.
	SummonerID int `json:"summonerId"`
	// Team id associated with player.
	TeamID TeamID `json:"teamId"`
}

// Obtains recent games for a given summoner id (max 10).
func (api *LolApi) RecentSummonerGames(id string) (*RecentGamesDto, *RestError) {
	result := &RecentGamesDto{}

	url := fmt.Sprintf(RECENT_SUMMONER_GAMES_URL, api.region.url,
		api.region.code, id)

	err := api.readFromApi(url, nil, result)
	if err != nil {
		return nil, err
	}

	return result, err
}
