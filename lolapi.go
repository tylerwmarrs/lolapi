// This package provides a means to deserialize JSON data from
// Riot's League of Legends API. Data can be consumed by the
// API directly or from JSON file stored on local disk.
package lolapi

import (
    "bytes"
	"encoding/json"
	"fmt"
    "io"
	"io/ioutil"
	"net/http"
	"simplecache"
	"strconv"
	"strings"
	"time"
)

// Options to be used when filtering data.
type ApiOpts map[string]string

// Struct that contains API key information and is used
// to fetch all data.
type LolApi struct {
	// The API key used to access Riot's API.
	key string
	// The region to gather data from.
	region *Region
	// The time in seconds when the rate limit resets
	rateLimitDuration int64
	// Last time a request was made to the server
	lastRequestTime int64
	// Simple cache to store data in memory.
	cache *simplecache.Cache
}

// Initialize values and get new instance.
func NewLolApi(key string, regionCode string) *LolApi {
	api := new(LolApi)
	api.cache = simplecache.NewCache()
	api.SetKey(key)
	api.SetRegion(regionCode)

	return api
}

// Simple helper that initializes cache of static data.
func (api *LolApi) WarmCache() {
	api.StaticAllChampions()
}

// Sets the API key.
func (api *LolApi) SetKey(key string) {
	api.key = key
}

// Sets the region by code.
func (api *LolApi) SetRegion(regionCode string) {
	r, err := NewRegionByCode(regionCode)
	if err != nil {
		panic(err)
	}
	api.region = r
}

// Getter for current region
func (api *LolApi) Region() *Region {
    return api.region
}

// Helper to format a given URL with query parameters
func (api *LolApi) formatURL(url string, options ApiOpts) string {
	res := fmt.Sprintf("%s?api_key=%s", url, api.key)
	for k, v := range options {
		res = res + "&" + k + "=" + v
	}
	return res
}

// Helper to get the time in seconds when the API
// will be available for non-static requests.
func (api *LolApi) apiAvailbleIn() int64 {
	if api.rateLimitDuration == 0 {
		return 0
	}

	passedTime := (time.Now().UnixNano() - api.lastRequestTime) / int64(time.Second)
	return passedTime - api.rateLimitDuration
}

// Helper to see if rate limit is still in effect.
func (api *LolApi) ApiAvailable() bool {
	return api.apiAvailbleIn() >= 0
}

// Helper to check if the given URL is static or not.
// Static URL's do not count against the rate limit.
func urlIsStatic(url string) bool {
	return strings.Contains(url, "static")
}

// Makes a web request and parses JSON into the given DTO.
// Note that the URL should be complete. The options allow
// for the query parameters to be constructed. The logic
// performs checks to ensure that rate limiting is performed.
// Essentially, when a 429 request is sent back we keep track
// of when we can make a non-static request again.
func (api *LolApi) readFromApi(url string, options ApiOpts, v interface{}) *RestError {
	// check for api key
	if api.key == "" {
		return &RestError{0, "Unable to fetch data: API key is not set."}
	}

	// check for rate limit
	if !urlIsStatic(url) && !api.ApiAvailable() {
		return &RestError{429, fmt.Sprintf("Currently rate limited and URL is not static. Available in %i seconds",
			api.apiAvailbleIn())}
	}

	// build URL and make request
	fullURL := api.formatURL(url, options)
	resp, err := http.Get(fullURL)

	// At this point a request was made so change the
	// last request time to now.
	if !urlIsStatic(url) {
		api.lastRequestTime = time.Now().UnixNano()
	}

	if err != nil {
        statusCode := 0
        if resp != nil {
            statusCode = resp.StatusCode
        }

		return &RestError{statusCode, fmt.Sprintf("Could not reach %s: %s", fullURL, err)}
	}

	// handle the rate limit status explicitly
	if resp.StatusCode == 429 {
        var e error
		api.rateLimitDuration, e = strconv.ParseInt(resp.Header.Get("Retry-After"), 10, 64)
        if e != nil {
            api.rateLimitDuration = 5
        }
		resp.Body.Close()
		return &RestError{429, "Too many server requests"}
	}

	// At this point we know that we are no longer
	// rate limited. We can reset the rate limit
	// duration to 0.
	if !urlIsStatic(url) {
		api.rateLimitDuration = 0
	}

	// handle any other bad responses
	if resp.StatusCode >= 400 {
        // make api available again in 5 seconds
        api.rateLimitDuration = 5
		resp.Body.Close()
		return &RestError{resp.StatusCode, fmt.Sprintf("Could not reach %s", fullURL)}
	}

    body, err := ioutil.ReadAll(resp.Body)
    switch {
    case err == io.EOF:
		return &RestError{0, "Got empty body"}
    case err != nil:
        return &RestError{0, err.Error()}
    }

	decoder := json.NewDecoder(bytes.NewReader(body))
	err = decoder.Decode(v)

    if err != nil {
        return &RestError{0, err.Error()}
    }

    resp.Body.Close()
	return nil
}

// Reads a local file and parses JSON into the given DTO.
func (api *LolApi) readFromFile(filePath string, dto interface{}) error {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	err = json.Unmarshal(content, dto)
	if err != nil {
		return err
	}

	return nil
}
