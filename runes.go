package lolapi

import (
	"fmt"
	"strings"
)

// Represents rune pages result from Riot's LoL API
type RunePagesDto struct {
	Pages      []RunePageDto `json:"pages"`
	SummonerId uint64        `json:"summonerId"`
}

// Represents a summoner's rune page giving the name
// and whether or not it is set as the current page.
type RunePageDto struct {
	Current bool          `json:"current"`
	ID      int           `json:"id"`
	Slots   []RuneSlotDto `json:"slots"`
	Name    string        `json:"name"`
}

// Represents a rune slot and what is in each slot
// for a specific rune page.
// Note that ID corresponds to the static API data.
type RuneSlotDto struct {
	ID         int `json:"runeId"`
	RuneSlotId int `json:"runeSlotId"`
}

// Provides a list of rune pages for the given summoner id.
func (api *LolApi) SummonerRunes(id string) ([]RunePageDto, *RestError) {
	res, err := api.SummonersRunes([]string{id})
	if err != nil {
		return nil, err
	}

	return res[id].Pages, err
}

// Provides rune pages for given summoner ids.
// Result is a map of string (summoner id) to pages.
func (api *LolApi) SummonersRunes(ids []string) (map[string]RunePagesDto, *RestError) {
	if len(ids) > 40 {
		return nil, &RestError{0, fmt.Sprintf("Request limited to 40 summoner IDs, %d requested", len(ids))}
	}
	if len(ids) == 0 {
		return nil, &RestError{0, fmt.Sprintf("You need to provide at least one Summoner ID")}
	}

	res := make(map[string]RunePagesDto)
	url := fmt.Sprintf("https://%s/api/lol/%s/v1.4/summoner/%s/runes",
		api.region.url, api.region.code, strings.Join(ids, ","))
	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}
