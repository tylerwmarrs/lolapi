package lolapi

import (
	"fmt"
	"strings"
)

// A Summoner is a representation of a player on LoL servers
type SummonerDto struct {
	ID            uint64           `json:"id"`
	Name          string           `json:"name"`
	ProfileIconID int              `json:"profileIconId"`
	Level         uint32           `json:"summonerLevel"`
	RevisionDate  EpochMillisecond `json:"revisionDate"`
}

// Gets a single summoner by their name
func (api *LolApi) SummonerByName(name string) (*SummonerDto, *RestError) {
	res, err := api.SummonersByNames([]string{name})
	if err != nil {
		return nil, err
	}

	return res[0], nil
}

// SummonersByNames returns Summoner data identified by their names
func (api *LolApi) SummonersByNames(names []string) ([]*SummonerDto, *RestError) {
	if len(names) > 40 {
		return nil, &RestError{0, fmt.Sprintf("Request limited to 40 summoner names, %d requested", len(names))}
	}
	if len(names) == 0 {
		return nil, &RestError{0, "You need to provide at least one Summoner name"}
	}

	res := make(map[string]*SummonerDto, len(names))
	url := fmt.Sprintf(SUMMONER_BY_NAME_URL, api.region.url, api.region.code,
		strings.Join(names, ","))
	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	actualRes := make([]*SummonerDto, 0, len(res))
	for _, v := range res {
		actualRes = append(actualRes, v)
	}

	return actualRes, err
}

// SummonersByIds returns Summoner data identified by their IDs
func (api *LolApi) SummonersByIds(ids []string) ([]*SummonerDto, *RestError) {
	if len(ids) > 40 {
		return nil, &RestError{0, fmt.Sprintf("Request limited to 40 summoner IDs, %d requested", len(ids))}
	}
	if len(ids) == 0 {
		return nil, &RestError{0, "You need to provide at least one Summoner ID"}
	}

	res := make(map[string]*SummonerDto, len(ids))
	url := fmt.Sprintf(SUMMONER_BY_ID_URL, api.region.url, api.region.code,
		strings.Join(ids, ","))
	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	actualRes := make([]*SummonerDto, 0, len(res))
	for _, v := range res {
		actualRes = append(actualRes, v)
	}

	return actualRes, err
}

// Gets a single summoner by their ID
func (api *LolApi) SummonerById(id string) (*SummonerDto, *RestError) {
	res, err := api.SummonersByIds([]string{id})
	if err != nil {
		return nil, err
	}

	return res[0], nil
}
