package lolapi

import (
	"fmt"
	"time"
)

// Riot's League of Legends API.
// This is the data structure for a champion's statistics.
// It includes armor, attack damage, crit etc...
type ChampionStatsDto struct {
	Armor                float32 `json:"armor"`
	ArmorPerLevel        float32 `json:"armorperlevel"`
	AttackDamage         float32 `json:"attackdamage"`
	AttackDamagePerLevel float32 `json:"attackdamageperlevel"`
	AttackRange          float32 `json:"attackrange"`
	AttackSpeedOffset    float32 `json:"attackspeedoffset"`
	AttackSpeedPerLevel  float32 `json:"attackspeedperlevel"`
	Crit                 float32 `json:"crit"`
	CritPerLevel         float32 `json:"critperlevel"`
	Hp                   float32 `json:"hp"`
	HpPerLevel           float32 `json:"hpperlevel"`
	HpRegen              float32 `json:"hpregen"`
	HpRegenPerLevel      float32 `json:"hpregenperlevel"`
	MoveSpeed            float32 `json:"movespeed"`
	Mp                   float32 `json:"mp"`
	MpPerLevel           float32 `json:"mpperlevel"`
	MpRegen              float32 `json:"mpregen"`
	MpRegenPerLevel      float32 `json:"mpregenperlevel"`
	SpellBlock           float32 `json:"spellblock"`
	SpellBlockPerLevel   float32 `json:"spellblockperlevel"`
}

// Riot's League of Legends API.
// This data structure provides information on the champion's
// general attack, defense, difficulty to play and magic damage.
type ChampionInfoDto struct {
	Attack     int `json:"attack"`
	Defense    int `json:"defense"`
	Difficulty int `json:"difficulty"`
	Magic      int `json:"magic"`
}

// Riot's League of Legends API.
// Provides information on a champion's passive ability.
type ChampionPassiveDto struct {
	Description          string   `json:"description"`
	Image                ImageDto `json:"image"`
	Name                 string   `json:"name"`
	SanitizedDescription string   `json:"sanitizedDescription"`
}

// Riot's League of Legends API.
// Data structure that encapsulates all of the champions
// spell information. One for q, w, e and r.
type ChampionSpellDto struct {
	AltImages    []ImageDto `json:"altimages"`
	Cooldown     []float32  `json:"cooldown"`
	CooldownBurn string     `json:"cooldownBurn"`
	Cost         []int      `json:"cost"`
	CostBurn     string     `json:"costBurn"`
	CostType     string     `json:"costType"`
	Description  string     `json:"description"`
	// effect
	// effectBurn
	Image    ImageDto `json:"image"`
	Key      string   `json:"key"`
	Leveltip struct {
		Labels []string `json:"label"`
	} `json:"leveltip"`
	MaxRank int    `json:"maxrank"`
	Name    string `json:"name"`

	// list of integers or self for spell casts that are self target
	Range                interface{} `json:"range"`
	RangeBurn            string      `json:"rangeBurn"`
	Resource             string      `json:"resource"`
	SanitizedDescription string      `json:"sanitizedDescriptions"`
	Tooltip              string      `json:"tooltip"`
	SanitizedTooltip     string      `json:"sanitizedTooltip"`
	//vars
}

// Riot's League of Legends API.
// General container for the champion including name, tips, etc.
type ChampionDto struct {
	AllyTips  []string           `json:"allytips"`
	Blurb     string             `json:"blurb"`
	EnemyTips []string           `json:"enemytips"`
	ID        int                `json:"id"`
	Image     ImageDto           `json:"image"`
	Info      ChampionInfoDto    `json:"info"`
	Key       string             `json:"key"`
	Lore      string             `json:"lore"`
	Name      string             `json:"name"`
	Partype   string             `json:"partype"`
	Passive   ChampionPassiveDto `json:"passive"`
	Spells    []ChampionSpellDto `json:"spells"`
	Stats     ChampionStatsDto   `json:"stats"`
	Tags      []string           `json:"tags"`
	Title     string             `json:"title"`
}

// Get champion information by id from API.
func (api *LolApi) StaticChampionById(id int) (*ChampionDto, *RestError) {
	cacheKey := fmt.Sprintf("champion-%d-%s", id, api.region.Code())
	result := &ChampionDto{}
	// check if local cache has copy
	if tmp, ok := api.cache.Get(cacheKey); ok {
		fmt.Println("Got from cache")
		return tmp.(*ChampionDto), nil
	}

	url := fmt.Sprintf(STATIC_CHAMPION_URL, api.region.Code(), id)
	options := make(map[string]string)
	options["champData"] = "all"

	err := api.readFromApi(url, options, result)
	if err != nil {
		return nil, err
	}

	api.cache.Set(cacheKey, result, time.Hour*24)
	return result, err
}
