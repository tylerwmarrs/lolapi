package lolapi

import (
	"fmt"
)

// Container for all champions
type RegionalChampionListDto struct {
	Champions []*RegionalChampionDto `json:"champions"`
}

// Champion information for a given region. This does not correlate to
// the stats etc. It only provides information on champion availability,
// free to play etc. You must manually map champion stats via this
// champion id to the static data.
type RegionalChampionDto struct {
	// Indicates if the champion is active.
	Active bool `json:"active"`
	// Bot enabled flag (for custom games).
	BotEnabled bool `json:"botEnabled"`
	// Bot Match Made enabled flag (for Co-op vs. AI games).
	BotMmEnabled bool `json:"botMmEnabled"`
	// Indicates if the champion is free to play. Free to play champions
	// are rotated periodically.
	FreeToPlay bool `json:"freeToPlay"`
	// Champion ID. For static information correlating to champion IDs,
	// please refer to the LoL Static Data API.
	ID int `json:"id"`
	// Ranked play enabled flag.
	RankedPlayEnabled bool `json:"rankedPlayEnabled"`
}

// Obtain all regional specific champion information.
// The result is a map of int (champion regional id) -> champ.
func (api *LolApi) AllChampions() (map[int]*RegionalChampionDto, *RestError) {
	res := new(RegionalChampionListDto)
	url := fmt.Sprintf(CHAMPIONS_URL, api.region.url, api.region.code)

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	actualRes := make(map[int]*RegionalChampionDto, len(res.Champions))
	for _, v := range res.Champions {
		actualRes[v.ID] = v
	}

	return actualRes, err
}

// Obtain regional specific champion information for given champion id.
// The result is a map of int (champion regional id) -> champ.
func (api *LolApi) Champion(id string) (*RegionalChampionDto, *RestError) {
	res := new(RegionalChampionDto)
	url := fmt.Sprintf(CHAMPION_URL, api.region.url, api.region.code, id)

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}
