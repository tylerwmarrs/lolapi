package lolapi

import (
	"fmt"
	"time"
)

// Riot API object that holds realm data.
type RealmDto struct {
	// The base CDN url.
	CDN string `json:"cdn"`
	// Latest changed version of Dragon Magic's css file.
	CSSVersion string `json:"css"`
	// Latest changed version of Dragon Magic.
	DDVersion string `json:"dd"`
	// Language for this realm.
	Language string `json:"l"`
	// Legacy script mode for IE6 or older.
	LegacyIE6 string `json:"lg"`
	// Latest changed version for each data type listed.
	Versions map[string]string `json:"n"`
	// Special behavior number identifying the largest profileicon id that
	// can be used under 500. Anything between this number and 500 should
	// be mapped to 0.
	ProfileIconMax int `json:"profileiconmax"`
	// Additional API data drawn from other sources that may be related
	// to data dragon functionality.
	//Store string `json:"store"`
	// Current version of this file for this realm.
	Version string `json:"v"`
}

// Get realm information for the current region that is set.
func (api *LolApi) Realm() (*RealmDto, *RestError) {
	cacheKey := fmt.Sprintf("realm-%s", api.region.Code())
	result := &RealmDto{}
	// check if local cache has copy
	if tmp, ok := api.cache.Get(cacheKey); ok {
		fmt.Println("Got from cache")
		return tmp.(*RealmDto), nil
	}

	url := fmt.Sprintf(STATIC_REALM_URL, api.region.Code())
	err := api.readFromApi(url, nil, result)
	if err != nil {
		return nil, err
	}

	api.cache.Set(cacheKey, result, time.Hour * 24)
	return result, err
}
