package lolapi

// Static all champions API URL
const STATIC_CHAMPIONS_URL string = "https://global.api.pvp.net/api/lol/static-data/%s/v1.2/champion"

// Static champion API URL
const STATIC_CHAMPION_URL string = "https://global.api.pvp.net/api/lol/static-data/%s/v1.2/champion/%d"

// Static real API URL
const STATIC_REALM_URL string = "https://global.api.pvp.net/api/lol/static-data/%s/v1.2/realm"

// Region specific champions URL
const CHAMPIONS_URL string = "https://%s/api/lol/%s/v1.2/champion"

// Region specific champion URL
const CHAMPION_URL string = "https://%s/api/lol/%s/v1.2/champion/%s"

// Looks up summoners by name
const SUMMONER_BY_NAME_URL string = "https://%s/api/lol/%s/v1.4/summoner/by-name/%s"

// Looks up summoners by ID
const SUMMONER_BY_ID_URL string = "https://%s/api/lol/%s/v1.4/summoner/%s"

// URL for recent summoner games
const RECENT_SUMMONER_GAMES_URL string = "https://%s/api/lol/%s/v1.3/game/by-summoner/%s/recent"

// URL for challenger league
const CHALLENGER_LEAGUE_URL string = "https://%s/api/lol/%s/v2.5/league/challenger"

// URL for masters league
const MASTER_LEAGUE_URL string = "https://%s/api/lol/%s/v2.5/league/master"

// URL for summoners league
const SUMMONER_LEAGUE_URL string = "https://%s/api/lol/%s/v2.5/league/by-summoner/%s"

// URL for teams league
const TEAM_LEAGUE_URL string = "https://%s/api/lol/%s/v2.5/league/by-team/%s"

// URL for champion mastery
const CHAMPION_MASTERY_URL string = "https://%s/championmastery/location/%s/player/%s/champions"

// URL for ranked summoner stats
const RANKED_SUMMONER_STATS_URL string = "https://%s/api/lol/%s/v1.3/stats/by-summoner/%s/ranked"

// URL for summarized summoner stats
const SUMMARIZED_SUMMONER_STATS_URL string = "https://%s/api/lol/%s/v1.3/stats/by-summoner/%s/summary"

// URL for summoner match history
const SUMMONER_MATCH_LIST_URL string = "https://%s/api/lol/%s/v2.2/matchlist/by-summoner/%s"

// URL for match details
const MATCH_URL string = "https://%s/api/lol/%s/v2.2/match/%s"

// URL for current game information on a summoner
const CURRENT_GAME_URL string = "https://%s/observer-mode/rest/consumer/getSpectatorGameInfo/%s/%s"
