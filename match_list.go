package lolapi

import (
	"fmt"
)

// This object contains match list information for the given summoner.
type MatchListDto struct {
	StartIndex int                 `json:"startIndex"`
	EndIndex   int                 `json:"endIndex"`
	Matches    []MatchReferenceDto `json:"matches"`
	TotalGames int                 `json:"totalGames"`
}

// This object contains match reference information.
type MatchReferenceDto struct {
	ChampionID int `json:"champion"`
	// Legal values: MID, MIDDLE, TOP, JUNGLE, BOT, BOTTOM
	Lane       string `json:"lane"`
	MatchID    uint64 `json:"matchId"`
	PlatformID string `json:"platformId"`
	// Legal values: TEAM_BUILDER_DRAFT_RANKED_5x5, RANKED_SOLO_5x5,
	// RANKED_TEAM_3x3, RANKED_TEAM_5x5
	Queue  string `json:"queue"`
	Region string `json:"region"`
	// Legal values: DUO, NONE, SOLO, DUO_CARRY, DUO_SUPPORT
	Role string `json:"role"`
	// Legal values: PRESEASON3, SEASON3, PRESEASON2014, SEASON2014,
	// PRESEASON2015, SEASON2015, PRESEASON2016, SEASON2016
	Season string           `json:"season"`
	Date   EpochMillisecond `json:"timestamp"`
}

// Fetches match list information for a given summoner id.
// This API call only allows for ranked game data. Additional filtering
// options are available. Please refer to the Riot API matchlist service.
func (api *LolApi) SummonerRankedMatches(id string, options ApiOpts) (*MatchListDto, *RestError) {
	res := new(MatchListDto)
	url := fmt.Sprintf(SUMMONER_MATCH_LIST_URL, api.region.url, api.region.code, id)

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}
