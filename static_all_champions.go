package lolapi

import (
	"fmt"
	"time"
)

// Riot's League of Legends API.
// Data structure that matches the all champions response.
type AllChampionsDto struct {
	Version   string                  `json:"version"`
	Champions map[string]*ChampionDto `json:"data"`
	Format    string                  `json:"format"`
	Type      string                  `json:"type"`
	Keys      map[string]string       `json:"keys"`
}

// Get all champion DTO from local file.
func (api *LolApi) AllChampionsFromFile(filePath string) (*AllChampionsDto, error) {
	result := &AllChampionsDto{}
	err := api.readFromFile(filePath, result)
	if err != nil {
		return nil, err
	}

	return result, err
}

// Get all champion DTO from API.
func (api *LolApi) StaticAllChampions() (*AllChampionsDto, *RestError) {
	cacheKey := fmt.Sprintf("staticchamps-%s", api.region.Code())
	result := &AllChampionsDto{}
	// check if local cache has copy
	if tmp, ok := api.cache.Get(cacheKey); ok {
		return tmp.(*AllChampionsDto), nil
	}

	url := fmt.Sprintf(STATIC_CHAMPIONS_URL, api.region.Code())
	options := make(map[string]string)
	options["champData"] = "all"

	err := api.readFromApi(url, options, result)
	if err != nil {
		return nil, err
	}

	api.cache.Set(cacheKey, result, time.Hour*24)
	return result, err
}
