package lolapi

import "time"

// EpochMillisecond represents a point in time by the number of
// milliseconds since EPOCH
type EpochMillisecond uint64

// Time converts EpochMillisecond to time.Time
func (s EpochMillisecond) Time() time.Time {
	secs := int64(s) / 1000
	return time.Unix(secs, int64(s)-secs)
}
