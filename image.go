package lolapi

// This is an Image data structure that is returned from Riot's
// League of Legends API.
type ImageDto struct {
	Full   string `json:"full"`
	Group  string `json:"group"`
	Height int    `json:"h"`
	Width  int    `json:"w"`
	Sprite string `json:"sprite"`
	X      int    `json:"x"`
	Y      int    `json:"y"`
}
