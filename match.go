package lolapi

import (
	"fmt"
	"strconv"
)

// This object contains match detail information.
type MatchDetailDto struct {
	MapId uint64 `json:"mapId"`
	// Match creation time. Designates when the team select lobby is created
	// and/or the match is made through match making, not when the game actually starts.
	MatchCreation EpochMillisecond `json:"matchCreation"`
	// How long the match lasted.
	MatchDuration uint64 `json:"matchDuration"`
	// ID of the match.
	MatchId uint64 `json:"matchId"`
	// Match mode (Legal values: CLASSIC, ODIN, ARAM, TUTORIAL, ONEFORALL,
	// ASCENSION, FIRSTBLOOD, KINGPORO)
	MatchMode string `json:"matchMode"`
	// Match type (Legal values: CUSTOM_GAME, MATCHED_GAME, TUTORIAL_GAME)
	MatchType string `json:"matchType"`
	// Match version
	MatchVersion string `json:"matchVersion"`
	// Participant identity information
	ParticipantIdentities []ParticipantIdentityDto `json:"participantIdentities"`
	// Participant information
	Participants []ParticipantDto `json:"participants"`
	// Platform ID of the match
	PlatformId string `json:"platformId"`
	// Match queue type (Legal values: CUSTOM, NORMAL_5x5_BLIND, RANKED_SOLO_5x5,
	// RANKED_PREMADE_5x5, BOT_5x5, NORMAL_3x3, RANKED_PREMADE_3x3, NORMAL_5x5_DRAFT,
	// ODIN_5x5_BLIND, ODIN_5x5_DRAFT, BOT_ODIN_5x5, BOT_5x5_INTRO, BOT_5x5_BEGINNER,
	// BOT_5x5_INTERMEDIATE, RANKED_TEAM_3x3, RANKED_TEAM_5x5, BOT_TT_3x3, GROUP_FINDER_5x5,
	// ARAM_5x5, ONEFORALL_5x5, FIRSTBLOOD_1x1, FIRSTBLOOD_2x2, SR_6x6, URF_5x5,
	// ONEFORALL_MIRRORMODE_5x5, BOT_URF_5x5, NIGHTMARE_BOT_5x5_RANK1, NIGHTMARE_BOT_5x5_RANK2,
	// NIGHTMARE_BOT_5x5_RANK5, ASCENSION_5x5, HEXAKILL, BILGEWATER_ARAM_5x5, KING_PORO_5x5,
	// COUNTER_PICK, BILGEWATER_5x5, TEAM_BUILDER_DRAFT_UNRANKED_5x5,
	// TEAM_BUILDER_DRAFT_RANKED_5x5)
	QueueType string `json:"queueType"`
	// Region where the match was played
	Region string `json:"region"`
	// Season match was played (Legal values: PRESEASON3, SEASON3, PRESEASON2014,
	// SEASON2014, PRESEASON2015, SEASON2015, PRESEASON2016, SEASON2016)
	Season string `json:"season"`
	// Team information
	Teams []TeamDto `json:"teams"`
	// Match timeline data (not included by default)
	Timeline TimelineDto `json:"timeline"`
}

// This object contains match participant information.
type ParticipantDto struct {
	// Champion ID
	ChampionID int `json:"championId"`
	// Highest ranked tier achieved for the previous season, if any, otherwise null. Used to
	// display border in game loading screen. (Legal values: CHALLENGER, MASTER, DIAMOND,
	// PLATINUM, GOLD, SILVER, BRONZE, UNRANKED)
	HighestAchievedSeasonTier string `json:"highestAchievedSeasonTier"`
	// List of mastery information
	Masteries []MasteryDto `json:"masteries"`
	// Participant ID
	ParticipantId int64 `json:"participantId"`
	// List of rune information
	Runes []RuneDto `json:"runes"`
	// First summoner spell ID
	Spell1Id int `json:"spell1Id"`
	// Second summoner spell ID
	Spell2Id int `json:"spell2Id"`
	// Participant statistics
	Stats ParticipantStatsDto `json:"stats"`
	// Team ID
	TeamId int `json:"teamId"`
	// Timeline data. Delta fields refer to values for the specified period (e.g.,
	// the gold per minute over the first 10 minutes of the game versus the second
	// 20 minutes of the game. Diffs fields refer to the deltas versus the calculated
	// lane opponent(s).
	Timeline ParticipantTimelineDto `json:"timeline"`
}

// Participant Identity for this match.
type ParticipantIdentityDto struct {
	// Participant ID
	ParticipantID int64 `json:"participantId"`
	// Player information
	Player MatchPlayerDto `json:"player"`
}

// This object contains team information
type TeamDto struct {
	// If game was draft mode, contains banned champion data, otherwise null
	Bans []BannedChampionDto `json:"bans"`
	// Number of times the team killed baron
	BaronKills int `json:"baronKills"`
	// If game was a dominion game, specifies the points the team had at game
	// end, otherwise null
	DominionVictoryScore int64 `json:"dominionVictoryScore"`
	// Number of times the team killed dragon
	DragonKills int `json:"dragonKills"`
	// Flag indicating whether or not the team got the first baron kill
	FirstBaron bool `json:"firstBaron"`
	// Flag indicating whether or not the team got first blood
	FirstBlood bool `json:"firstBlood"`
	// Flag indicating whether or not the team got the first dragon kill
	FirstDragon bool `json:"firstDragon"`
	// Flag indicating whether or not the team destroyed the first inhibitor
	FirstInhibitor bool `json:"firstInhibitor"`
	// Flag indicating whether or not the team got the first rift herald kill
	FirstRiftHerald bool `json:"firstRiftHerald"`
	// Flag indicating whether or not the team destroyed the first tower
	FirstTower bool `json:"firstTower"`
	// Number of inhibitors the team destroyed
	InhibitorKills int `json:"inhibitorKills"`
	// Number of times the team killed rift herald
	RiftHeraldKills int `json:"riftHeraldKills"`
	// Team ID
	TeamID TeamID `json:"teamId"`
	// Number of towers the team destroyed
	TowerKills int `json:"towerKills"`
	// Number of times the team killed vilemaw
	VilemawKills int `json:"vilemawKills"`
	// Flag indicating whether or not the team won
	Winner bool `json:"winner"`
}

type TimelineDto struct{
    // Time between each returned frame in milliseconds.
    FrameInterval int64 `json:"frameInterval"`
    // List of timeline frames for the game.
    Frames []FrameDto `json:"frames"`
}

type FrameDto struct {
    // List of events for this frame.
    Events []EventDto `json:"events"`
    // Map of each participant ID to the participant's information for the frame.
    ParticipantFrames map[string]ParticipantFrameDto `json:"participantFrames"`
    // Represents how many milliseconds into the game the frame occurred.
    Timestamp int64 `json:"timestamp"`
}

// This object contains game event information. Note that not all legal type values documented below are valid for all games. Event data evolves over time and certain values may be relevant only for older or newer games.
type EventDto struct {
    // The ascended type of the event. Only present if relevant. Note that CLEAR_ASCENDED refers to when a participants kills the ascended player. (Legal values: CHAMPION_ASCENDED, CLEAR_ASCENDED, MINION_ASCENDED)
    AscendedType string `json:"ascendedType"`
    // The assisting participant IDs of the event. Only present if relevant.
    AssistingParticipantIDs []int64 `json:assistingParticipantIds"`
    // The building type of the event. Only present if relevant. (Legal values: INHIBITOR_BUILDING, TOWER_BUILDING)
    BuildingType string `json:"buildingType"`
    // The creator ID of the event. Only present if relevant.
    CreatorID int64 `json:"creatorId"`
    // Event type. (Legal values: ASCENDED_EVENT, BUILDING_KILL, CAPTURE_POINT, CHAMPION_KILL, ELITE_MONSTER_KILL, ITEM_DESTROYED, ITEM_PURCHASED, ITEM_SOLD, ITEM_UNDO, PORO_KING_SUMMON, SKILL_LEVEL_UP, WARD_KILL, WARD_PLACED)
    EventType string `json:"eventType"`
    // The ending item ID of the event. Only present if relevant.
    ItemAfter int `json:"itemAfter"`
    // The starting item ID of the event. Only present if relevant.
    ItemBefore int `json:"itemBefore"`
    // The item ID of the event. Only present if relevant.
    ItemId int `json:"itemId"`
    // The killer ID of the event. Only present if relevant. Killer ID 0 indicates a minion.
    KillerID int64 `json:"killerId"`
    // The lane type of the event. Only present if relevant. (Legal values: BOT_LANE, MID_LANE, TOP_LANE)
    LaneType string `json:"laneType"`
    // The level up type of the event. Only present if relevant. (Legal values: EVOLVE, NORMAL)
    LevelUpType string `json:"levelUpType"`
    // The monster type of the event. Only present if relevant. (Legal values: BARON_NASHOR, BLUE_GOLEM, DRAGON, RED_LIZARD, RIFTHERALD, VILEMAW)
    MonsterType	string `json:"monsterType"`
    // The participant ID of the event. Only present if relevant.
    ParticipantID int64 `json:"participantId"`
    // The point captured in the event. Only present if relevant. (Legal values: POINT_A, POINT_B, POINT_C, POINT_D, POINT_E)
    PointCaptured string `json:"pointCaptured"`
    // The position of the event. Only present if relevant.
    Position PositionDto `json:"position"`
    // The skill slot of the event. Only present if relevant.
    SkillSlot int `json:"skillShot"`
    // The team ID of the event. Only present if relevant.
    TeamID int `json:"teamId"`
    // Represents how many milliseconds into the game the event occurred.
    Timestamp int64 `json:"timestamp"`
    // The tower type of the event. Only present if relevant. (Legal values: BASE_TURRET, FOUNTAIN_TURRET, INNER_TURRET, NEXUS_TURRET, OUTER_TURRET, UNDEFINED_TURRET)
    TowerType string `json:"towerType"`
    // The victim ID of the event. Only present if relevant.
    VictimID int64 `json:"victimId"`
    // The ward type of the event. Only present if relevant. (Legal values: BLUE_TRINKET, SIGHT_WARD, TEEMO_MUSHROOM, UNDEFINED, VISION_WARD, YELLOW_TRINKET, YELLOW_TRINKET_UPGRADE)
    WardType string	`json:"wardType"`
}

// This object contains participant frame position information
type PositionDto struct {
    X int64 `json:"x"`
    Y int64 `json:"y"`
}

// Contains match player information
type MatchPlayerDto struct{
    MatchHistoryUri string `json:"matchHistoryUri"`
    ProfileIcon int `json:"profileIcon"`
    SummonerID uint64 `json:"summonerId"`
    SummonerName string `json:"summonerName"`
}

// TODO: implement
type RuneDto struct{}
type ParticipantStatsDto struct{}
type ParticipantTimelineDto struct{}
type ParticipantFrameDto struct{}

// Fetch a match given a match ID.
// Not all matches have timeline data. If timeline data is requested, but doesn't exist,
// then the response won't include it.
func (api *LolApi) Match(id string, timeline bool) (*MatchDetailDto, *RestError) {
	res := new(MatchDetailDto)
	url := fmt.Sprintf(MATCH_URL, api.region.url, api.region.code, id)
	options := make(map[string]string)
	options["includeTimeline"] = strconv.FormatBool(timeline)

	err := api.readFromApi(url, options, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

/*
Implementation Notes

Mastery - This object contains mastery information
Name	Data Type	Description
masteryId	long	Mastery ID
rank	long	Mastery rank

ParticipantStats - This object contains participant statistics information
Name	Data Type	Description
assists	long	Number of assists
champLevel	long	Champion level achieved
combatPlayerScore	long	If game was a dominion game, player's combat score, otherwise 0
deaths	long	Number of deaths
doubleKills	long	Number of double kills
firstBloodAssist	boolean	Flag indicating if participant got an assist on first blood
firstBloodKill	boolean	Flag indicating if participant got first blood
firstInhibitorAssist	boolean	Flag indicating if participant got an assist on the first inhibitor
firstInhibitorKill	boolean	Flag indicating if participant destroyed the first inhibitor
firstTowerAssist	boolean	Flag indicating if participant got an assist on the first tower
firstTowerKill	boolean	Flag indicating if participant destroyed the first tower
goldEarned	long	Gold earned
goldSpent	long	Gold spent
inhibitorKills	long	Number of inhibitor kills
item0	long	First item ID
item1	long	Second item ID
item2	long	Third item ID
item3	long	Fourth item ID
item4	long	Fifth item ID
item5	long	Sixth item ID
item6	long	Seventh item ID
killingSprees	long	Number of killing sprees
kills	long	Number of kills
largestCriticalStrike	long	Largest critical strike
largestKillingSpree	long	Largest killing spree
largestMultiKill	long	Largest multi kill
magicDamageDealt	long	Magical damage dealt
magicDamageDealtToChampions	long	Magical damage dealt to champions
magicDamageTaken	long	Magic damage taken
minionsKilled	long	Minions killed
neutralMinionsKilled	long	Neutral minions killed
neutralMinionsKilledEnemyJungle	long	Neutral jungle minions killed in the enemy team's jungle
neutralMinionsKilledTeamJungle	long	Neutral jungle minions killed in your team's jungle
nodeCapture	long	If game was a dominion game, number of node captures
nodeCaptureAssist	long	If game was a dominion game, number of node capture assists
nodeNeutralize	long	If game was a dominion game, number of node neutralizations
nodeNeutralizeAssist	long	If game was a dominion game, number of node neutralization assists
objectivePlayerScore	long	If game was a dominion game, player's objectives score, otherwise 0
pentaKills	long	Number of penta kills
physicalDamageDealt	long	Physical damage dealt
physicalDamageDealtToChampions	long	Physical damage dealt to champions
physicalDamageTaken	long	Physical damage taken
quadraKills	long	Number of quadra kills
sightWardsBoughtInGame	long	Sight wards purchased
teamObjective	long	If game was a dominion game, number of completed team objectives (i.e., quests)
totalDamageDealt	long	Total damage dealt
totalDamageDealtToChampions	long	Total damage dealt to champions
totalDamageTaken	long	Total damage taken
totalHeal	long	Total heal amount
totalPlayerScore	long	If game was a dominion game, player's total score, otherwise 0
totalScoreRank	long	If game was a dominion game, team rank of the player's total score (e.g., 1-5)
totalTimeCrowdControlDealt	long	Total dealt crowd control time
totalUnitsHealed	long	Total units healed
towerKills	long	Number of tower kills
tripleKills	long	Number of triple kills
trueDamageDealt	long	True damage dealt
trueDamageDealtToChampions	long	True damage dealt to champions
trueDamageTaken	long	True damage taken
unrealKills	long	Number of unreal kills
visionWardsBoughtInGame	long	Vision wards purchased
wardsKilled	long	Number of wards killed
wardsPlaced	long	Number of wards placed
winner	boolean	Flag indicating whether or not the participant won

ParticipantTimeline - This object contains all timeline information
Name	Data Type	Description
ancientGolemAssistsPerMinCounts	ParticipantTimelineData	Ancient golem assists per minute timeline counts
ancientGolemKillsPerMinCounts	ParticipantTimelineData	Ancient golem kills per minute timeline counts
assistedLaneDeathsPerMinDeltas	ParticipantTimelineData	Assisted lane deaths per minute timeline data
assistedLaneKillsPerMinDeltas	ParticipantTimelineData	Assisted lane kills per minute timeline data
baronAssistsPerMinCounts	ParticipantTimelineData	Baron assists per minute timeline counts
baronKillsPerMinCounts	ParticipantTimelineData	Baron kills per minute timeline counts
creepsPerMinDeltas	ParticipantTimelineData	Creeps per minute timeline data
csDiffPerMinDeltas	ParticipantTimelineData	Creep score difference per minute timeline data
damageTakenDiffPerMinDeltas	ParticipantTimelineData	Damage taken difference per minute timeline data
damageTakenPerMinDeltas	ParticipantTimelineData	Damage taken per minute timeline data
dragonAssistsPerMinCounts	ParticipantTimelineData	Dragon assists per minute timeline counts
dragonKillsPerMinCounts	ParticipantTimelineData	Dragon kills per minute timeline counts
elderLizardAssistsPerMinCounts	ParticipantTimelineData	Elder lizard assists per minute timeline counts
elderLizardKillsPerMinCounts	ParticipantTimelineData	Elder lizard kills per minute timeline counts
goldPerMinDeltas	ParticipantTimelineData	Gold per minute timeline data
inhibitorAssistsPerMinCounts	ParticipantTimelineData	Inhibitor assists per minute timeline counts
inhibitorKillsPerMinCounts	ParticipantTimelineData	Inhibitor kills per minute timeline counts
lane	string	Participant's lane (Legal values: MID, MIDDLE, TOP, JUNGLE, BOT, BOTTOM)
role	string	Participant's role (Legal values: DUO, NONE, SOLO, DUO_CARRY, DUO_SUPPORT)
towerAssistsPerMinCounts	ParticipantTimelineData	Tower assists per minute timeline counts
towerKillsPerMinCounts	ParticipantTimelineData	Tower kills per minute timeline counts
towerKillsPerMinDeltas	ParticipantTimelineData	Tower kills per minute timeline data
vilemawAssistsPerMinCounts	ParticipantTimelineData	Vilemaw assists per minute timeline counts
vilemawKillsPerMinCounts	ParticipantTimelineData	Vilemaw kills per minute timeline counts
wardsPerMinDeltas	ParticipantTimelineData	Wards placed per minute timeline data
xpDiffPerMinDeltas	ParticipantTimelineData	Experience difference per minute timeline data
xpPerMinDeltas	ParticipantTimelineData	Experience per minute timeline data

Rune - This object contains rune information
Name	Data Type	Description
rank	long	Rune rank
runeId	long	Rune ID

Player - This object contains match player information
Name	Data Type	Description
matchHistoryUri	string	Match history URI
profileIcon	int	Profile icon ID
summonerId	long	Summoner ID
summonerName	string	Summoner name
BannedChampion - This object contains information about banned champions
Name	Data Type	Description
championId	int	Banned champion ID
pickTurn	int	Turn during which the champion was banned

ParticipantFrame - This object contains participant frame information
Name	Data Type	Description
currentGold	int	Participant's current gold
dominionScore	int	Dominion score of the participant
jungleMinionsKilled	int	Number of jungle minions killed by participant
level	int	Participant's current level
minionsKilled	int	Number of minions killed by participant
participantId	int	Participant ID
position	Position	Participant's position
teamScore	int	Team score of the participant
totalGold	int	Participant's total gold
xp	int	Experience earned by participant


ParticipantTimelineData - This object contains timeline data
Name	Data Type	Description
tenToTwenty	double	Value per minute from 10 min to 20 min
thirtyToEnd	double	Value per minute from 30 min to the end of the game
twentyToThirty	double	Value per minute from 20 min to 30 min
zeroToTen	double	Value per minute from the beginning of the game to 10 min
*/
