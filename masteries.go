package lolapi

import (
	"fmt"
	"strings"
)

// Represents mastery pages result from Riot's LoL API
type MasteryPagesDto struct {
	Pages      []MasteryPageDto `json:"pages"`
	SummonerId uint64           `json:"summonerId"`
}

// Represents a summoner's mastery page giving the name
// and whether or not it is set as the current page.
type MasteryPageDto struct {
	Current   bool         `json:"current"`
	ID        int          `json:"id"`
	Masteries []MasteryDto `json:"masteries"`
	Name      string       `json:"name"`
}

// Represents a mastery and number of points it contains
// for a specific master page.
// Note that ID corresponds to the static API data.
type MasteryDto struct {
	ID   int `json:"id"`
	Rank int `json:"rank"`
}

// Provides a list of mastery pages for the given summoner id.
func (api *LolApi) SummonerMasteries(id string) ([]MasteryPageDto, *RestError) {
	res, err := api.SummonersMasteries([]string{id})
	if err != nil {
		return nil, err
	}

	return res[id].Pages, err
}

// Provides mastery pages for given summoner ids.
// Result is a map of string (summoner id) to pages.
func (api *LolApi) SummonersMasteries(ids []string) (map[string]MasteryPagesDto, *RestError) {
	if len(ids) > 40 {
		return nil, &RestError{0, fmt.Sprintf("Request limited to 40 summoner IDs, %d requested", len(ids))}
	}
	if len(ids) == 0 {
		return nil, &RestError{0, fmt.Sprintf("You need to provide at least one Summoner ID")}
	}

	res := make(map[string]MasteryPagesDto)
	url := fmt.Sprintf("https://%s/api/lol/%s/v1.4/summoner/%s/masteries",
		api.region.url, api.region.code, strings.Join(ids, ","))
	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}
