package lolapi

import (
	"fmt"
)

// Information about a game that is currently being played.
type CurrentGameInfoDto struct {
	// Banned champions list.
	BannedChampions []BannedChampionDto `json:"bannedChampions"`
	// ID of the game.
	GameID uint64 `json:"gameId"`
	// The amount of time in seconds that has passed since the game started
	TimeElapsed uint `json:"gameLength"`
	// The game mode (Legal values: CLASSIC, ODIN, ARAM, TUTORIAL, ONEFORALL,
	// ASCENSION, FIRSTBLOOD, KINGPORO)
	GameMode string `json:"gameMode"`
	// The queue type (queue types are documented on the Game Constants page)
	GameQueueID QueueID `json:"gameQueueConfigId"`
	// The game start time represented in epoch milliseconds
	GameStartTime EpochMillisecond `json:"gameStartTime"`
	// The game type (Legal values: CUSTOM_GAME, MATCHED_GAME, TUTORIAL_GAME)
	GameType string `json:"gameType"`
	// The ID of the map
	MapID uint `json:"mapId"`
	// The observer information
	Observer struct {
		EncryptionKey string `json:"encryptionKey"`
	} `json:"observers"`
	// The participant information
	Participants []CurrentGameParticipantDto `json:"participants"`
	// The ID of the platform on which the game is being played
	PlatformID string `json:"platformId"`
}

// Information on the who, what and when on a champion ban.
type BannedChampionDto struct {
	// The ID of the banned champion
	ChampionID int `json:"championId"`
	// The turn during which the champion was banned
	PickTurn uint `json:"pickTurn"`
	// The ID of the team that banned the champion
	TeamID TeamID `json:"teamId"`
}

// Information for a player of the current game.
type CurrentGameParticipantDto struct {
	// Flag indicating whether or not this participant is a bot
	IsBot bool `json:"bot"`
	// The ID of the champion played by this participant
	ChampionID int `json:"championId"`
	// The masteries used by this participant
	Masteries []CurrentGameMasteryDto `json:"masteries"`
	// The ID of the profile icon used by this participant
	ProfileIconID int `json:"profileIconId"`
	// The runes used by this participant
	Runes []CurrentGameRuneDto `json:"runes"`
	// The ID of the first summoner spell used by this participant
	SummonerSpell1ID int `json:"spell1Id"`
	// The ID of the second summoner spell used by this participant
	SummonerSpell2ID int `json:"spell2Id"`
	// The summoner ID of this participant
	SummonerID uint64 `json:"summonerId"`
	// The summoner name of this participant
	SummonerName string `json:"summonerName"`
	// The team ID of this participant, indicating the participant's team
	TeamID TeamID `json:"teamId"`
}

// Details on a participant's given mastery for the current game.
type CurrentGameMasteryDto struct {
	// The ID of the mastery
	MasteryID int `json:"masteryId"`
	// The number of points put into this mastery by the user
	Rank int `json:"rank"`
}

// Details on a participant's given runes for the current game.
type CurrentGameRuneDto struct {
	// The count of this rune used by the participant
	Count int `json:"count"`
	// The ID of the rune
	RuneID int `json:"runeId"`
}

// Look up current game by summoner ID.
func (api *LolApi) SummonerCurrentGame(id string) (*CurrentGameInfoDto, *RestError) {
	res := new(CurrentGameInfoDto)
	url := fmt.Sprintf(CURRENT_GAME_URL, api.region.url, api.region.platformID, id)

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}
