package lolapi

import (
	"fmt"
)

// Ranked stats for a summoner.
type RankedStatsDto struct {
	// Collection of aggregated stats summarized by champion.
	ChampionStats []SummonerChampionStatsDto `json:"champions"`
	// Date stats were last modified specified as epoch milliseconds.
	ModifyDate EpochMillisecond `json:"modifyDate"`
	// Summoner ID.
	SummonerID uint64 `json:"summonerId"`
}

// A collection of summoner champion stats.
type SummonerChampionStatsDto struct {
	// Champion ID. Note that champion ID 0 represents the combined
	// stats for all champions. For static information correlating
	// to champion IDs, please refer to the LoL Static Data API.
	ChampionID int `json:"id"`
	// Aggregated stats associated with the champion.
	Stats AggregatedStatsDto `json:"stats"`
}

// his object contains a collection of player stats summary information.
type PlayerStatsSummaryListDto struct {
	// Collection of player stats summaries associated with the summoner.
	Summaries []PlayerStatsSummaryDto `json:"playerStatSummaries"`
	// Summoner ID.
	SummonerID uint64 `json:"summonerId"`
}

// This object contains player stats summary information.
type PlayerStatsSummaryDto struct {
	// Aggregated stats.
	Stats AggregatedStatsDto `json:"aggregatedStats"`
	// Number of losses for this queue type. Returned for ranked queue
	// types only.
	Losses int `json:"losses"`
	// Date stats were last modified specified as epoch milliseconds.
	ModifyDate EpochMillisecond `json:"modifyDate"`
	// Player stats summary type. (Legal values: AramUnranked5x5,
	// Ascension, Bilgewater, CAP5x5, CoopVsAI, CoopVsAI3x3,
	// CounterPick, FirstBlood1x1, FirstBlood2x2, Hexakill, KingPoro,
	// NightmareBot, OdinUnranked, OneForAll5x5, RankedPremade3x3,
	// RankedPremade5x5, RankedSolo5x5, RankedTeam3x3, RankedTeam5x5,
	// SummonersRift6x6, Unranked, Unranked3x3, URF, URFBots)
	QueueType string `json:"playerStatSummaryType"`
	// Number of wins for this queue type.
	Wins int `json:"wins"`
}

// Multitude of aggregated stats for a summoner on a champion or
// for a given summoner.
type AggregatedStatsDto struct {
	// Dominion specific stats.
	Dominion struct {
		AverageAssists              int `json:"averageAssists"`
		AverageChampionsKilled      int `json:"averageChampionsKilled"`
		AverageCombatPlayerScore    int `json:"averageCombatPlayerScore"`
		AverageNodeCapture          int `json:"averageNodeCapture"`
		AverageNodeCaptureAssist    int `json:"averageNodeCaptureAssist"`
		AverageNodeNeutralize       int `json:"averageNodeNeutralize"`
		AverageNodeNeutralizeAssist int `json:"averageNodeNeutralizeAssist"`
		AverageNumDeaths            int `json:"averageNumDeaths"`
		AverageObjectivePlayerScore int `json:"averageObjectivePlayerScore"`
		AverageTeamObjective        int `json:"averageTeamObjective"`
		AverageTotalPlayerScore     int `json:"averageTotalPlayerScore"`
		MaxAssists                  int `json:"maxAssists"`
		MaxCombatPlayerScore        int `json:"maxCombatPlayerScore"`
		MaxNodeCapture              int `json:"maxNodeCapture"`
		MaxNodeCaptureAssist        int `json:"maxNodeCaptureAssist"`
		MaxNodeNeutralize           int `json:"maxNodeNeutralize"`
		MaxNodeNeutralizeAssist     int `json:"maxNodeNeutralizeAssist"`
		MaxObjectivePlayerScore     int `json:"maxObjectivePlayerScore"`
		MaxTeamObjective            int `json:"maxTeamObjective"`
		MaxTotalPlayerScore         int `json:"maxTotalPlayerScore"`
		TotalNodeCapture            int `json:"totalNodeCapture"`
		TotalNodeNeutralize         int `json:"totalNodeNeutralize"`
	}
	BotGamesPlayed              int `json:"botGamesPlayed"`
	KillingSpree                int `json:"killingSpree"`
	MaxChampionsKilled          int `json:"maxChampionsKilled"`
	MaxLargestCriticalStrike    int `json:"maxLargestCriticalStrike"`
	MaxLargestKillingSpree      int `json:"maxLargestKillingSpree"`
	MaxNumDeaths                int `json:"maxNumDeaths"`
	MaxTimePlayed               int `json:"maxTimePlayed"`
	MaxTimeSpentLiving          int `json:"maxTimeSpentLiving"`
	MostChampionKillsPerSession int `json:"mostChampionKillsPerSession"`
	MostSpellsCast              int `json:"mostSpellsCast"`
	NormalGamesPlayed           int `json:"normalGamesPlayed"`
	RankedPremadeGamesPlayed    int `json:"rankedPremadeGamesPlayed"`
	RankedSoloGamesPlayed       int `json:"rankedSoloGamesPlayed"`
	TotalAssists                int `json:"totalAssists"`
	TotalChampionKills          int `json:"totalChampionKills"`
	TotalDamageDealt            int `json:"totalDamageDealt"`
	TotalDamageTaken            int `json:"totalDamageTaken"`
	TotalDeathsPerSession       int `json:"totalDeathsPerSession"`
	TotalDoubleKills            int `json:"totalDoubleKills"`
	TotalFirstBlood             int `json:"totalFirstBlood"`
	TotalGoldEarned             int `json:"totalGoldEarned"`
	TotalHeal                   int `json:"totalHeal"`
	TotalMagicDamageDealt       int `json:"totalMagicDamageDealt"`
	TotalMinionKills            int `json:"totalMinionKills"`
	TotalNeutralMinionsKilled   int `json:"totalNeutralMinionsKilled"`
	TotalPentaKills             int `json:"totalPentaKills"`
	TotalPhysicalDamageDealt    int `json:"totalPhysicalDamageDealt"`
	TotalQuadraKills            int `json:"totalQuadraKills"`
	TotalSessionsLost           int `json:"totalSessionsLost"`
	TotalSessionsPlayed         int `json:"totalSessionsPlayed"`
	TotalSessionsWon            int `json:"totalSessionsWon"`
	TotalTripleKills            int `json:"totalTripleKills"`
	TotalTurretsKilled          int `json:"totalTurretsKilled"`
	TotalUnrealKills            int `json:"totalUnrealKills"`
}

// Win ratio over total games won vs played.
func (s *AggregatedStatsDto) WinRate() float64 {
	return (float64(s.TotalSessionsWon) / float64(s.TotalSessionsPlayed)) * 100
}

// Lose ratio over total games lost vs played.
func (s *AggregatedStatsDto) LoseRate() float64 {
	return (float64(s.TotalSessionsLost) / float64(s.TotalSessionsPlayed)) * 100
}

// Average gold earned per game.
func (s *AggregatedStatsDto) AverageGoldEarned() float64 {
	return float64(s.TotalGoldEarned) / float64(s.TotalSessionsPlayed)
}

// Obtain ranked summoner stats aggregated per champion.
func (api *LolApi) SummonerStatsRanked(id string) (*RankedStatsDto, *RestError) {
	res := new(RankedStatsDto)
	url := fmt.Sprintf(RANKED_SUMMONER_STATS_URL, api.region.url,
		api.region.code, id)

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}

// Obtain summarized summoner stats per queue type.
func (api *LolApi) SummonerStatsSummary(id string) (*PlayerStatsSummaryListDto, *RestError) {
	res := new(PlayerStatsSummaryListDto)
	url := fmt.Sprintf(SUMMARIZED_SUMMONER_STATS_URL, api.region.url,
		api.region.code, id)

	err := api.readFromApi(url, nil, &res)
	if err != nil {
		return nil, err
	}

	return res, err
}
