package lolapi

import (
	"fmt"
)

// Custom error type that provides HTTP status code.
// The LoL API provides status codes for some services that should be handled
// in a certain way.
type RestError struct {
	StatusCode int
	Message    string
}

// Implements the Error interface.
func (e *RestError) Error() string {
	if e.StatusCode != 0 {
		return fmt.Sprintf("Status Code: %d - %s", e.StatusCode, e.Message)
	}
	return fmt.Sprintf("Unable to process request: %s", e.Message)
}
